import React from 'react';
//import ReactDOM from 'react-dom';

const Title = ({todoCount}) => {
  return (
    <div>
      <h1>React Todo App ({todoCount})</h1>
    </div>
  );
}

const TodoForm = ({addTodo}) => {
  let input = null;

  return (
    <div>
      <input ref={node => {input = node;}} />
      <button onClick={() => {
        addTodo(input.value);
        input.value = '';
      }}>
      +
      </button>
    </div>
  );
}

const Todo = ({todo, remove}) => {
  return (<li onClick={() => {remove(todo.id)}}>{todo.text}</li>);
}

const TodoList = ({todos, remove}) => {
  // Map through the todos
  const todoNode =  todos.map((todo) => {
    return (<Todo todo={todo} key={todo.id} remove={remove} />)
  });

  return (<ul>{todoNode}</ul>);
}

let id = 0;
class TodoReactApp extends React.Component {
  constructor(props) {
    super(props);
    // set initial state
    this.state = {
      todos: []
    }
    // bind event handlers
    this.addTodoHandler = this.addTodo.bind(this);
    this.removeTodoHandler = this.removeTodo.bind(this);

  } // constructor

  addTodo(val) {
    // assemble data
    const todo = {text: val, id: id++};

    // add new todo
    let newTodos = this.state.todos.concat([todo]);

    //update state
    this.setState({todos: newTodos});
  }

  removeTodo(id){
    // filter all todos except the one to be removed
    const remainder = this.state.todos.filter((todo) => {
      if (todo.id !== id) return todo;
    });

    // update state
    this.setState({todos: remainder});
  }

  render(){
    return (
      <div className="App">
        <p className="App-intro">
          <Title todoCount={this.state.todos.length}/>
          <TodoForm addTodo={this.addTodoHandler} />
          <TodoList todos={this.state.todos} remove={this.removeTodoHandler} />
        </p>
      </div>
    );
  }
}

export default TodoReactApp;
