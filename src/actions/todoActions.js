let todoId = 0

export const addTodo = (text) => {
    return {
      type: 'TODO_ADD',
      id: ++todoId,
      todo: text
    }
  }

export const deleteTodo = (id) => {
    return {
      type: 'TODO_DELETE',
      id: id
    }
}
