import React from 'react'
import AddTodo from '../containers/AddTodoContainer'
import VisibleTodoList from '../containers/VisibleTodoListContainer'


const TodoReduxApp = () => (
  <div>
    <AddTodo />
    <VisibleTodoList />
  </div>
)

export default TodoReduxApp
