import React from 'react'
import PropTypes from 'prop-types'

const Todo = ({onClick, todo}) => (
  <li onClick={onClick}>
    {todo}
  </li>
)

Todo.propTypes = {
  onClick: PropTypes.func.isRequired,
  todo: PropTypes.string.isRequired
}

export default Todo
