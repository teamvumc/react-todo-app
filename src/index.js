import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import TodoReactApp from './TodoApp';
import './index.css';
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import todoAppReducer from './reducers/todoAppReducer'
import TodoReduxApp from './components/TodoAppComponent'

ReactDOM.render(
  <App />,
  document.getElementById('root')
);

ReactDOM.render(
  <TodoReactApp />,
  document.getElementById('react-app')
);

let store = createStore(todoAppReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

ReactDOM.render(
  <Provider store={store}>
    <TodoReduxApp />
  </Provider>,
  document.getElementById('redux-app')
)
