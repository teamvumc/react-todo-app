import {combineReducers} from 'redux'
import todos from './todosReducer'

const todoAppReducer = combineReducers({
  todos
})

export default todoAppReducer
