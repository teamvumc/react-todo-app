
const initialState = [{
  id: 0,
  todo: 'Use Redux'
}]

const todos = (state = initialState, action) => {
  switch (action.type) {
    case 'TODO_ADD':
      return [{
        id: action.id,
        todo: action.todo
      }, ...state]

    case 'TODO_DELETE':
      return state.filter(
        todo => todo.id !== action.id
      )

    default:
       return state
  }
}

export default todos
